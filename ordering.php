<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Заказ</title>
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/bootstrap-grid.min.css">
    <link rel="stylesheet" href="styles/custom/ordering.css">
</head>

<?php
require_once(__DIR__.'/sql/connection.php');

// Connection to MySQLI.
$mySqliConnect = new MySqliConnect();
$msql = $mySqliConnect->msql();
// для декодирования longblob изображений из БД
$image = new Imagick();
// перестройка ид
$mySqliConnect->rebuildTableId('sells');

//================================
//часть для отображения страницы запроса
//================================
?>
<body class="body-ordering">
<!--убираем с принтера-->
<span class="noprint">
<div class='row w-100 justify-content-center'>
    <a class ="print-doc mx-3" href="javascript:(print());"> <img src="картинки/ico/print.png" alt=”Распечатать” width="50px" height="50px"></a>
    <button type="button" class='btn btn-success col-2 font-weight-bold' name='home' onclick="location.href = 'index.php'">На главную</button>
</div>
</span>
<?php
//удаление товара
$order_ids = $msql->query("SELECT `id_tovara` FROM `ordering`")->fetch_all();
foreach ($order_ids as $key=>$order_id){
    $del_order='delete_'.$order_id[0];
    if (isset($_POST[$del_order])) {
        $delete_order = $msql->query("DELETE FROM `ordering` WHERE `id_tovara`='$order_id[0]'");
//        если в заказах пусто виход на главную
        $order_ids = $msql->query("SELECT * FROM `ordering`")->fetch_all();
        if (empty($order_ids)){
            header("Location: index.php");
            exit();
        }
    }
}

//оформление заказа
$order_sql = $msql->query("SELECT * FROM `ordering`")->fetch_all();

if (isset($_POST['confirm'])) {
    $sell_date = $_POST['date'];
    $total_price = $_POST['total'];
    if (empty($order_sql)) {
        echo "<div class='row justify-content-center mt-5'><h1 class='text-danger'>Заказов нет! Сделайте заказ.</h1></div>";
        exit();
    }
//    устанавливаем новое количество товара после покупки
    foreach ($_POST as $key=>$count){
        if (strpos($key,'count_')!==false){
           $idTovaraOrder = explode('_',$key)[1];
            foreach ($order_sql as $index=>$order) {
                $id_tov = $order[1];
                if ($idTovaraOrder == $id_tov ) {
                    $count_sql = $msql->query("SELECT `count` FROM `tovar` WHERE `id`='$idTovaraOrder'")->fetch_array();
                    $new_count = ($count_sql[0] - $count);
                    $msql->query("UPDATE `tovar` SET `count`= '$new_count' WHERE `id`='$idTovaraOrder' ");
                }
            }
        }
    }

// очщаем таблицу заказа
    $clear_order = $msql->query("DELETE FROM `ordering`");
    setcookie("sell", "done");
    header("Location: index.php");
}
?>

<section id="ordering">
    <form method="post">
        <table class='table table-success table-hover'>
            <caption class="text-white h4 bg-dark text-center"></caption>
            <thead class='table-dark '>
            <tr class=''><th scope='col' colspan='7' class='h4 text-white bg-dark text-center'>Список заказа</th></tr>
            <tr class=''>
                <th scope='col' class='rounded-pill order-head text-center'>Наименование:</th>
                <th scope='col' class='rounded-pill order-head text-center'>Описание:</th>
                <th scope='col' class='rounded-pill order-head text-center'>Количество:</th>
                <th scope='col' class='rounded-pill order-head text-center'>Цена:</th>
                <span class='noprint'>
                    <th scope='col' colspan='2' class='rounded-pill order-head text-center noprint'>Удалить:</th>
                </span>
            </tr>
            </thead>
            <tbody>
<?php
            foreach ($order_sql as $key=>$item) {
                $count[] = $item[4];
                $sum[] = $item[5];
                $totalCount = array_sum($count);
                $total = array_sum($sum);
                echo"<tr>";
                echo"
                      <th scope='row' class='name rounded-pill text-center p-1 pt-3'>$item[2]</th>
                      <td colspan='1' class='description'>$item[3]</td>
               
                      <td class='rounded-pill text-center p-1 pt-3'>
                       <span class='noprint'>
                            <input type='button' class='text-center p-0 border border-success'style='width: 24px; height: 24px;' id='btn_plus' operation='+' id_tovara='$item[1]'' value='+' >
                       </span>
                       <input type='text' name='count_$item[1]' class='count w-25 text-center p-0 border-0' id='count_$item[1]' count='$item[4]' id_tovara='$item[1]' value='1'>
                       <span class='noprint'>
                            <input type='button' class='text-center p-0 border border-success' style='width: 24px; height: 24px;' id='btn_minus' operation='-' id_tovara='$item[1]'' value='-' >
                       </span>
                      </td>
                      
                      <input type='text' id='total' hidden name='total'>
                      <td class='price rounded-pill text-center p-1 pt-3' id='price_$item[1]' data-value='$item[5]'>$item[5]</td>
                      
                      <td class='text-center' colspan='2'>
                          <span class='noprint'>
                            <button type='submit' name='delete_$item[1]' class='btn-delete btn-danger rounded-pill '>Удалить</button>
                          </span>
                      </td>
                </tr>
        ";}
            echo "<tr>
        <thead class='table-dark'>
        <tr>
            <th scope='col' colspan='2'class='rounded-pill order-head'>Информация о заказе:</th>
            <th scope='col' class='rounded-pill order-head text-center'>Всего:</th>
            <th scope='col' colspan='3' class='rounded-pill order-head text-center'>Всего:</th>
        </tr>
        </thead>
            <td class='table-active rounded-pill' colspan='2'>Ваш заказ составляет:</td>
            <td class='table-active rounded-pill text-center' id='total_count'> <input type='text' name='count' id='count' hidden></td>
            <td class='table-active text-center pr-1' colspan='2' id='total_sum'> <input type='text' name='total' id='total' hidden></td>
        </tr>
        ";
?>
            </tbody>
        </table>
        <span class="noprint">
            <div class="row d-flex justify-content-center">
                <input type='text' id='date' name='date' hidden value=''>
                <button id="agree" name="confirm" class="bg-dark border border-info col-3 col-lg-3 col-xl-2 text-white text-center h3 rounded-pill py-2" type="submit">Подтвердить</button>
            </div>
        </span>
    </form>
</section>


<script src="scripts/jquery-3.6.0.min.js"></script>
<script src="scripts/jquery.cookie.js"></script>
<script src="scripts/bootstrap.bundle.min.js"></script>
<script src="scripts/custom/current-date.js"></script>
<script src="scripts/custom/counter.js"></script>

</body>

</html>