﻿function heyoliver_load_static_file() {
    try {
        if (!document.body) {
            clearInterval(ho_body_load_file_interval_timer);
            ho_body_load_file_interval_timer = setInterval(function () {
                heyoliver_load_static_file()
            }, 100);
            return
        }
        clearInterval(ho_body_load_file_interval_timer);
        var d = window.navigator && window.navigator.appVersion.split("MSIE");
        if (parseFloat(d[1])) {
            d = parseFloat(d[1])
        }
        var b = (_hoid.version) ? (heyopath + "/min/") : "https://www.heyoliver.com/webroot/ho-ui/v2/";
        var a = document.createElement("script");
        a.type = "text/javascript";
        a.async = true;
        a.src = b + "ho-loader.js?v=4" + ((d && d == 10) ? "?t=" + new Date().getTime() : "");
        document.getElementsByTagName("body")[0].appendChild(a)
    } catch (c) { }
}
var ho_body_load_file_interval_timer;
(function () {
    heyoliver_load_static_file()
})();