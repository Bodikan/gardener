<?php

//namespace Gardener\Connection;

// Include log file.
ini_set('mysql_error_log', 'mysql_error_log');

/**
 * Create class for getting MYSQLI connection with db.
 *
 * @todo improve new better connection function
 * @version 1.0
 * @package TMetric
 * @category TimeKeeper
 * @author Bogdan UA <godinmyhurt@gmail.com>
 * @copyright Copyright (c) 2020, Bogdan UA
 */
class MySqliConnect {

    /**
     * Function for connect with MySqli.
     *
     * @return \mysqli
     *   $mysqli.
     */
    public function msql() {
        $servername = 'db';
        $username = 'root';
        $password = 'root';
        $dbname = 'garden';

        $mysqli = new \mysqli($servername, $username, $password, $dbname);

        if (!$mysqli) {
            print 'Ошибка: Невозможно установить соединение с MySQL.' . \PHP_EOL;
            print 'Код ошибки errno: ' . mysqli_connect_errno() . \PHP_EOL;
            print 'Текст ошибки error: ' . mysqli_connect_error() . \PHP_EOL;

            exit;
        }
        // For message .
        // echo "Соединение с MySQL установлено!" . PHP_EOL;
        // echo "Информация о сервере: " . mysqli_get_host_info($mysqli) . PHP_EOL;
        // mysqli_close($mysqli);
        return $mysqli;
    }

    /**
     * Function for reset MYSQLI AUTO_INCREMENT.
     *
     * @param string $table
     */
    public function resetTableId(string $table) {
        $this->msql()->query("ALTER TABLE `{$table}` AUTO_INCREMENT = 1");
    }

    /**
     * Function for reset MYSQLI AUTO_INCREMENT.
     *
     * @param array $tables
     */
    public function resetTablesId(array $tables) {
        foreach ($tables as $key=>$table) {
            $this->msql()->query("ALTER TABLE `{$table}` AUTO_INCREMENT = 1");
        }
    }
    /**
     * Function for clear MYSQLI tables.
     *
     * @param array $tables
     */
    public function clearTables(array $tables) {
        foreach ($tables as $key=>$table) {
            $this->msql()->query("TRUNCATE `{$table}` ");
        }
    }

//    /**
//     * Function for rebuild MYSQLI AUTO_INCREMENT for ID name.
//     *
//     * @param string $mysqlTable
//     * @param string $id_row
//     */
//    public function rebuildTableIdName(string $mysqlTable, string $id_row) {
//        $this->msql()->query("ALTER TABLE `{$mysqlTable}` DROP `{$id_row}`");
//        $this->msql()->query("ALTER TABLE `{$mysqlTable}` ADD `{$id_row}` BIGINT( 200 ) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`{$id_row}`)");
//    }

    /**
     * Function for rebuild MYSQLI AUTO_INCREMENT ID.
     *
     * @param string $mysqlTable
     */
    public function rebuildTableId(string $mysqlTable) {
        $this->msql()->query("ALTER TABLE `{$mysqlTable}` DROP `id`");
        $this->msql()->query("ALTER TABLE `{$mysqlTable}` ADD `id` BIGINT( 200 ) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`)");
    }

    /**
     * Function for rebuild MYSQLI tables AUTO_INCREMENT ID.
     *
     * @param array $mysqlTables
     */
    public function rebuildTablesId(array $mysqlTables) {
        foreach ($mysqlTables as $key=>$mysqlTable) {
            $this->msql()->query("ALTER TABLE `{$mysqlTable}` DROP `id`");
            $this->msql()->query("ALTER TABLE `{$mysqlTable}` ADD `id` BIGINT( 200 ) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`)");
        }
    }

    /**
     * Function for update MYSQLI element.
     *
     * @param string $mysqlTable
     * @param string $mySqlWhatChange
     * @param string $mySqlWhereChange
     */
    public function updateElement(string $mysqlTable, string $mySqlWhatChange,
                               string $mySqlWhereChange) {
        // Divide $mySqlWhatChange and $mySqlWhereChange for needed format.
        $whatwhere = $mySqlWhatChange."=".$mySqlWhereChange;
        $explodeUpdate = explode('=', $whatwhere);
        $this->msql()->query("UPDATE `$mysqlTable` set `$explodeUpdate[0]`='$explodeUpdate[1]' where `$explodeUpdate[2]`='{$explodeUpdate[3]}'");
    }

    /**
     * Function for update MYSQLI row.
     *
     * @param string $mysqlTable
     * @param array $mySqlWhatChange
     * @param array $mySqlWhatValue
     * @param string $mySqlWhereChange
     * @param string $mySqlWhereValue
     */
    public function updateRow(string $mySqlTable, array $mySqlWhatChange, array $mySqlWhatValue,
                              string $mySqlWhereChange, string $mySqlWhereValue) {
        $i = 0;
        foreach ($mySqlWhatChange as $key=>$item) {
            $this->msql()->query("UPDATE `".$mySqlTable."` set `".$item."`='".$mySqlWhatValue[$i]."' where `".$mySqlWhereChange."`='".$mySqlWhereValue."'");
        $i++;
        }
    }


}
