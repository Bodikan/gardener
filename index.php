<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="title" content="Gardener">
    <!--    for google analytics -->
    <meta name="description"
          content="ITech4Web is a middle size specialized web development company based in Kyiv, the capital of Ukraine.">
    <meta name="keywords" content="Drupal,php,web,develop,bootstrap,java script">
    <!--    To allow a search engine crawler to index an HTML site-->
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="language" content="Russian">
    <meta name="viewport" content="width=device-width user-scalable=no, initial-scale=1.0"/>
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="apple-mobile-web-app-capable" content="yes"/>

    <title>Gardener</title>

    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/custom/main.css">
    <link rel="stylesheet" href="styles/custom/filter.css">
    <link rel="stylesheet" href="styles/bootstrap-grid.min.css">

</head>

<body>

<?php
require_once(__DIR__ . '/sql/connection.php');

// Connection to MySQLI.
$mySqliConnect = new MySqliConnect();
$msql = $mySqliConnect->msql();

$categorySQL = $msql->query('SELECT * FROM `category` ')->fetch_all();
$tovarSQL = $msql->query('SELECT * FROM `tovar` ')->fetch_all();

// select data
$sql_all_tovar = $msql->query("SELECT * FROM `tovar` INNER JOIN `category` using(`id`)")->fetch_all();

//    переход на страницу заказа
if (isset($_POST['order'])) {
    header("Location: ordering.php");
}
if (isset($_POST['all'])) {
    header("Location: list.php");
}
?>

<div class="container">
    <nav class="navbar navbar-expand-xl fixed-top navbar-dark bg-dark ">
            <a class="navbar-brand ml-5" href="#" id="all_tovar">Все товары</a>
            <ul class="main-menu nav justify-content-center mr-auto ml-auto">
                <li class="nav-item active">
                    <a class="show-category text-right nav-link" type="link"  id="menu-toggle" data-toggle="collapse" data-target="#category-view" aria-controls="category-view" aria-expanded="false" aria-label="Категории товара">
                        Категории товара
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Меню
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenu">
                        <a type="button" class="dropdown-item" id="add_new">Добавить товар</a>
                    </div>
                </li>
                <li class='nav-item'>
                    <form method='post'>
                        <input id='all' name='all' type='submit' class='mx-1 nav-link text-primary border-0' style='outline: none; background: none;' value="Список товара">
                    </form>
                </li>
                <?php
                //    отображение кнопки  заказа
                $order = $msql->query("SELECT * FROM `ordering`")->fetch_all();
                if(!empty($order)){
                    echo "
                <li class='nav-item'>
                    <form method='post'>
                        <button id='order' name='order' type='submit' class='btn_add btn btn-info m-1'>Оформить заказ</button>
                    </form>
                </li>
                ";}
                ?>
            </ul>
    </nav>
</div>

<!--Категории-->
<section id="category-view" class="my-2 collapse navbar-collapse w-100">
    <div class="card-group " id="#card-group">
        <div class='row justify-content-center w-100'>
        <?php
        foreach($categorySQL as $key=>$category) {
echo "    
    <div class='category-card card text-white bg-dark mb-2 border border-info rounded mx-xl-2 mx-1 px-xl-2 px-1 text-center' id-category='$category[0]' id='$category[0]' '>   
        <div class='col-img p-1 justify-content-center text-center w-100'>
          <img src='$category[2]' class='category-card-img border border-info rounded-pill p-2' alt='$category[1]'>
        </div>
        <div class='card-body p-1'>       
        <form action='' method='post'>
          <input type='text' hidden name='category_$category[0]'>
          <div class='category-head card-header p-1'>$category[1]</div>
        </form>
        </div>
    </div> 
";
        }
    ?>
        </div>
    </div>
</section>

<!--    модальное окно-->
<div class='modal fade' id='imgModal' tabindex='-1' role='dialog' aria-labelledby='modalPicture'
     aria-hidden='true' data-bs-backdrop='static'>
    <div class='modal-dialog modal-dialog-centered col-8'>
        <div class='modal-content rounded-pill' id='modalPicture'>
            <!--button close-->
            <div class='row'>
                <img id="modal_img" class="w-100 rounded-pill">
            </div>
        </div>
    </div>
</div>

<!--Карусель-->
<section id="carousel">
<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="картинки/3.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
                <h5>First slide label</h5>
                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </div>
        </div>
        <div class="carousel-item">
            <img src="картинки/23-2.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block text-dark">
                <h5>Third slide label</h5>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
            </div>
        </div>
        <div class="carousel-item">
            <img src="картинки/15-2.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block text-dark">
                <h5>Third slide label</h5>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
</section>

<!--Товары-->
<section id="tovar-view" class="my-2">
        <div class='row row-cols-1 row-cols-md-2 px-4'>
        <?php
        foreach ($tovarSQL as $key => $sort) {
echo "
    <div class='tovar-card col mb-3 px-lg-2 px-xl-3' id-category='$sort[0]' id-tovara='$sort[1]'>
      <div class='card h-100 bg-dark text-white text-center border border-primary rounded col-12 py-1 px-2' '>
        <div class='row no-gutters'>
        
          <div class='col-md-5 px-1 py-5'>
                <img src='$sort[2]' id='img_$sort[1]' class='card-img-top border border-info rounded-pill col-12'  alt='$sort[3]' data-toggle='modal' data-target='#imgModal'>
          </div>
          
          <div class='card-body col-7'>
    
              <form id='form_$sort[1]'  method='post'>
              
              <input type='text' id='$sort[0]' hidden name='id_category' value='$sort[0]'>
              <input type='text' id='$sort[1]' hidden name='id_tovar' value='$sort[1]'>

      
              <input type='text' hidden name='change_$sort[1]' value='$sort[1]'>
              <input type='text' name='add_$sort[1]' hidden style='width: 0px; height: 0px; border: none; outline: none;' value='$sort[1]'>
              
              <span class='font-weight-bold text-info' style='font-size: 16px;'>№ $sort[1] </span>
              
                <input type='text' class='card-title border-0 text-white' name='title_$sort[1]' contenteditable='true' value='$sort[3]' style='outline: none; background: none;'>
                <textarea class='card-description card-text bg-secondary rounded col-12 border-0 text-white' name='description_$sort[1]' contenteditable='true'  style='outline: none; background: none;'>$sort[4]</textarea>
                
                <div class='card-footer px-1'>
                  <small class='text-warning small'>Количество: </small><input type='text' name='count_$sort[1]' id='count' class='border-0 text-info' value='$sort[6]' style='outline: none; background: none; width: 30px;'>
                    <label for='price_$sort[1]' class='m-0'><small class='text-warning small'>Цена:</small></label>
                    <input type='text' name='price_$sort[1]' id='price' class='text-success text-right border-0 h6' value='$sort[7]' style='outline: none; background: none; width: 30px;'>грн.
                </div>

                <button id='change_$sort[1]' name='change_$sort[1]' type='button' id_tovara='$sort[1]' class='btn_change btn btn-success m-1' value='$sort[1]'>Изменить</button>
                <button id='add_$sort[1]' name='add_$sort[1]' type='button' id_tovara='$sort[1]' class='btn_add btn btn-light m-1' value='$sort[1]'>в корзину</button>
              </form>
              
          </div>
          
        </div>
      </div>
    </div>
";
        }
        ?>
    </div>
</section>

<script src="scripts/jquery-3.6.0.min.js"></script>
<script src="scripts/bootstrap.bundle.min.js"></script>
<script src="scripts/jquery.cookie.js"></script>
<script src="scripts/custom/index.js"></script>

</body>

</html>