form_list = $( "#filter-tovar" );

btn_category = $("#btn-category");
categoryList = $("#category-list");
category = $(".category-elem");

btn_subcategory = $("#btn-subcategory");
subcategoryList = $("#subcategory-list");
subcategory = $(".subcategory-elem");

btn_tovar = $("#btn-tovar");
tovarList = $("#tovar-list");
tovar = $(".tovar-elem");

// array of checkboxes CATEGORY
var categoryCheckArray =  $( "#filter-tovar" ).find( $(".category-elem").children());
// array of SUBCATEGORY elements Li with checkboxes
var subcategoryArray =  $( "#filter-tovar" ).find( $(".subcategory-elem") );
// array of TOVAR elements Li with checkboxes
var tovarArray =  $( "#filter-tovar" ).find( $(".tovar-elem") );

// ========================
// ========================
// show element
function showElement(element) {
    element.css({'display': 'block', 'visibility': 'visible'});
}
// ========================
// ========================
// hide element
function hideElement(element) {
    element.css({'display': 'none', 'visibility': 'hidden'});
}

