// add current date
function GetTodayDate() {
    var tdate = new Date();
    var dd = tdate.getDate(); //yields day
    var MM = tdate.getMonth()+1; //yields month
    if (MM < 10) {
        MM = "0" + MM;
    }
    var yyyy = tdate.getFullYear(); //yields year
    return dd + "." + (MM) + "." + yyyy;
}
$("#date").text(GetTodayDate());
$("#date").attr('value', GetTodayDate());