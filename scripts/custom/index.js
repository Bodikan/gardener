// ==================================================
// show element
function showElement(element) {
    element.css({'display': 'block', 'visibility': 'visible'});
}
// hide element
function hideElement(element) {
    element.css({'display': 'none', 'visibility': 'hidden'});
}
// ==================================================
$('#add_new').click(function (){
    location.href = 'add.php';
})
// ==================================================
// модальное окно
$cards_img = $('#tovar-view').find($('img.card-img-top'));
$cards_img.each(function (key, img){
    $(img).click(function (){
        img_id = parseInt($(this).val());
        img_card_src = $(this).attr('src');
        img_card_alt = $(this).attr('alt');
        $('#modal_img').attr('src', img_card_src);
        $('#modal_img').attr('alt', img_card_alt);
    })
})
// ==================================================
// показать кнопку изменения названия товара
$titleArray = $('.tovar-card').find($('input.card-title'));
$titleArray.each(function (key, title) {
    id_title = $(title).attr('name').split('_')[1];
    $(title).change(function (){
        console.log(title)
        id_t = $(this).attr('name').split('_')[1];
        $('#change_'+id_t).css({'display': 'inline-block', 'visibility': 'visible'});
        $('#change_'+id_t).click(function (){
            $.ajax({
                url: 'update.php', // путь к обработчику
                type: 'POST', // метод передачи данных
                dataType: 'json', // формат данных ожидаемых в ответе
                data: $("#form_"+id_c).serialize(),
            })
            if ($.cookie('change')=='changed'){
                // $.removeCookie('count');
                $.removeCookie('change');
                $('#change_'+id_t).css({'display': 'none', 'visibility': 'hidden'});
            }
        })
    })
})
// ==================================================
// показать кнопку изменения описания товара
$descriptionArray = $('.tovar-card').find($('textarea.card-description'));
$descriptionArray.each(function (key, description) {
    id_description = $(description).attr('name').split('_')[1];
    $(description).change(function (){

        id_d = $(this).attr('name').split('_')[1];
        $('#change_'+id_d).css({'display': 'inline-block', 'visibility': 'visible'});
        $('#change_'+id_d).click(function (){
            $.ajax({
                url: 'update.php', // путь к обработчику
                type: 'POST', // метод передачи данных
                dataType: 'json', // формат данных ожидаемых в ответе
                data: $("#form_"+id_c).serialize(),
            })
            if ($.cookie('change')=='changed'){
                $.removeCookie('change');
                $('#change_'+id_d).css({'display': 'none', 'visibility': 'hidden'});
            }
        })
    })
})
// ==================================================
// показать кнопку изменения количества товара
$countsArray = $('.tovar-card').find($('input#count'));
$countsArray.each(function (key, count) {
    id_count = $(count).attr('name').split('_')[1];
    $(count).change(function (){
        id_c = $(this).attr('name').split('_')[1];
        $('#change_'+id_c).css({'display': 'inline-block', 'visibility': 'visible'});
        $('#change_'+id_c).click(function (){
            $.ajax({
                url: 'update.php', // путь к обработчику
                type: 'POST', // метод передачи данных
                dataType: 'json', // формат данных ожидаемых в ответе
                data: $("#form_"+id_c).serialize(),
            })
            if ($.cookie('change')=='changed'){
                $.removeCookie('change');
                $('#change_'+id_c).css({'display': 'none', 'visibility': 'hidden'});
            }
        })
    })
})
// ==================================================
// показать кнопку изменения суммы товара
$pricesArray = $('.tovar-card').find($('input#price'));
$btn_priceArray = $('.tovar-card').find($('.btn_addPrice'));
$btn_priceArray.each(function (key, btn){
    id_btn = parseInt($(btn).attr('id_tovara'));
})
$pricesArray.each(function (key, price) {
    id_price = $(price).attr('name').split('_')[1];
    $(price).change(function (){
        id_p = $(this).attr('name').split('_')[1];
        $('#change_'+id_p).css({'display': 'inline-block', 'visibility': 'visible'});
        $('#change_'+id_p).click(function (){
            $.ajax({
                url: 'update.php', // путь к обработчику
                type: 'POST', // метод передачи данных
                dataType: 'json', // формат данных ожидаемых в ответе
                data: $("#form_"+id_p).serialize(),
            })
            if ($.cookie('change')=='changed'){
                $.removeCookie('change');
                $('#change_'+id_p).css({'display': 'none', 'visibility': 'hidden'});
            }
        })
    })
})
// ==================================================
// ==================================================
// фильтр товаров по вибраним категориям
$categories = $('section#category-view').find($('.category-card'));
$cards = $('section#tovar-view').find($('.tovar-card'));

$categories.each(function (key, category){
    $(category).click(function (){
        // ид категории
        id_category = parseInt($(category).attr('id-category'));
        $cards.each(function (key, tovar){
            // ид категории
            id_tov_category = parseInt($(tovar).attr('id-category'));
            if (id_category == id_tov_category){
                $(tovar).css('-webkit-transition','all 1.2s cubic-bezier(0, 0, .58, 1)');
                $(tovar).css('transition','all 1.2s cubic-bezier(0, 0, .58, 1)');
                $(tovar).show();
            }else{
                $(tovar).hide();
            }
        })
    })
})
// ==================================================
// фильтр все товари
$('#all_tovar').click(function (){
    $.each($cards, function (){
        showElement($(this));
    })
})
// ==================================================
btn_add_Array = $('section#tovar-view').find($('button.btn_add'));

$.each(btn_add_Array, function (key, btn_add) {
    id_btn = parseInt($(this).attr('id_tovara'));

    $('#add_'+id_btn).click(function(){
        id_btn = parseInt($(this).attr('id_tovara'));
        console.log(this)
        var artId = $(this).attr('id'); // id статьи в базе данных) {
        $.ajax({
            url: 'order.php', // путь к обработчику
            type: 'POST', // метод передачи данных
            dataType: 'json', // формат данных ожидаемых в ответе
            data: $("#form_"+id_btn).serialize(),
        });
        // если php кнопки нет то добавляем при нажатии
        if ($('button#order').length==0) {
            //создать и показать кнопку формления заказа
            // скрипт php показует кнопку только когда база не пуста(перезагрузка страници)
            var nav_li = document.createElement("li");
            $(nav_li).addClass('nav-item');
            var nav_form = document.createElement("form");
            $(nav_form).attr('method', 'post');
            var nav_button = document.createElement("button");
            $(nav_button).addClass('btn_add btn btn-info m-1');
            $(nav_button).attr('name', 'order');
            $(nav_button).attr('id', 'order');
            $(nav_button).attr('type', 'submit');
            $(nav_button).text('Оформить заказ');
            $(nav_li).append(nav_form);
            $(nav_form).append(nav_button);
            $('ul.main-menu ').append(nav_li);
        }

    });
});
// ==================================================
// вивод сообщения после покупки
if ($.cookie('sell')=='done'){
    alert("Спвсибо за покупку!")
    $.removeCookie('sell');
}