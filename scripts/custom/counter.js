// ======================
// добваление изначалого заказа
$(document).ready(function (){
    // всего колличества
    $counts_array = $('#ordering').find($('input.count'));
    $count = 0;
    $counts_array.each(function (key, count){
        $c = parseInt($(count).val());
        $count += $c;
    })
    // всего сума
    $prices_array = $('#ordering').find($('td.price'));
    price_sum = 0;
    $prices_array.each(function (key, price){
        $p = parseInt($(price).text());
        price_sum += $p;
    })

    // всего количество
    $('#total_count').text($count)
    $('#count').attr('value', $count);
    // всего цена
    $('#total_sum').text(price_sum+' грн.')
    $('input#total').attr('value', price_sum);
})

// ======================
// добавление счетчика товара
btn_plus_Array = $('section#ordering').find($('input#btn_plus'));
// btn_plus_Array = $('.group').find($('input#btn_plus'));
$.each(btn_plus_Array, function (key, btn_plus) {
    // кнопка добавить количество товара на карточке
    // клик срабативает в масиве кнопок только там где нажимать кнопку, другие не нажати
    $(btn_plus).click(function (){
        // ид товара
        id_tovara = parseInt($(this).attr('id_tovara'));
        // количество етого товара в базе
        count = parseInt($("#count_"+id_tovara).attr('count'));
        // значение инпута где количество товара
        value = parseInt($("#count_"+id_tovara).val());
        // если значение меньше количества товара в базе то добавляем
        if( $(this).attr('operation') == '+' && value < count ){
            input_count_tov = parseInt($("#count_"+id_tovara).val());
            $("#count_"+id_tovara).val(input_count_tov+1);

            // добавление цени
            $price = $('#price_'+id_tovara).attr('data-value');
            $new_price = $('#price_'+id_tovara).text($price*(value+1));

            // всего колличества
            $counts_array = $('#ordering').find($('input.count'));
            $count = 0;
            $counts_array.each(function (key, count){
                $c = parseInt($(count).val());
                $count += $c;
            })
            // всего сума
            $prices_array = $('#ordering').find($('td.price'));
            price_sum = 0;
            $prices_array.each(function (key, price){
                $p = parseInt($(price).text());
                price_sum += $p;
            })

            // всего количество
            $('#total_count').text($count)
            $('#count').attr('value', $count);
            // всего цена
            $('#total_sum').text(price_sum+' грн.')
            $('input#total').attr('value', price_sum);
        } else {
            return false;
        }
    })
})

// ======================
// отнимание счетчика товара
btn_minus_Array = $('section#ordering').find($('input#btn_minus'));
// btn_minus_Array = $('.group').find($('input#btn_minus'));
$.each(btn_minus_Array, function (key, btn_minus) {
    // кнопка уменьшить количество товара на карточке
    $(btn_minus).click(function (){
        // ид товара
        id_tovara = parseInt($(this).attr('id_tovara'));
        // значение инпута где количество товара
        value = parseInt($("#count_"+id_tovara).val());
        // если значение больше ноля то уменьшаем
        if( $(this).attr('operation') == '-' && value > 0 ){
            input_count_tov = parseInt($("#count_"+id_tovara).val());
            $("#count_"+id_tovara).val(input_count_tov-1);

            // уменьшение цени
            $new_price = $('#price_'+id_tovara).text();
            $price = $('#price_'+id_tovara).attr('data-value');
            $('#price_'+id_tovara).text($new_price - $price);

            // всего колличества
            $count = parseInt($('#total_count').text());
            $('#total_count').text($count - 1)
            $('#count').attr('value', $count - 1);

            // всего сума
            $prices_array = $('#ordering').find($('td.price'));
            price_sum = 0;
            $prices_array.each(function (key, price){
                $p = parseInt($(price).text());
                price_sum += $p;
            })
            // всего цена
            $('#total_sum').text(price_sum+' грн.')
            $('input#total').attr('value', price_sum);

        } else {
            return false;
        }
    });
})
