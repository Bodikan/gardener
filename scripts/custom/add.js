// Стилизация кнопки загрузки файлов
$(document).ready(function () {
    $('.bootstrap-filestyle').addClass('col-10 col-sm-12 col-md-10 ');
    $('.input-group-btn>label').removeClass('btn-secondary');
    $('.input-group-btn>label').addClass('btn-success');
    $('label>.buttonText').text("Фото товара:");
    // $('.input-group-btn>label').addClass('btn-primary');
    $('.input-group-btn').detach().insertBefore($('.form-control'));
})

// ========================
// show element
function showElement(element) {
    element.css({'display': 'block', 'visibility': 'visible'});
}
// ========================
// hide element
function hideElement(element) {
    element.css({'display': 'none', 'visibility': 'hidden'});
}
// ========================
$category = $('#category').find($('.category'));
$('#input_category').change(function (){
    $cat_val = $('#input_category').val();
    $cat_options = $('#category').children();
    $cat_options.each(function (key,cat_opt){
        $opt_id = $(cat_opt).attr('id');
        $opt_val = $(cat_opt).val();
        if ($cat_val === $opt_val){
            $('#input_category').attr('cat-id', $opt_id);
        }
    })
    $cat_id =  $('#input_category').attr('cat-id');

    $sub_options = $('#subcategory').children();
    $sub_options.each(function (key,sub_opt){
        $sub_opt_id = $(sub_opt).attr('sub-id');
        $opt_id = $(sub_opt).attr('id');
        $opt_val = $(sub_opt).val();

    })

})