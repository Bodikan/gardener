<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Создание товара</title>
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/bootstrap-grid.min.css">
    <link rel="stylesheet" href="styles/custom/add.css">
</head>

<?php
require_once(__DIR__ . '/sql/connection.php');

// Connection to MySQLI.
$mySqliConnect = new MySqliConnect();
$msql = $mySqliConnect->msql();

$category_sql = $msql->query("SELECT `id`,`name` FROM `category`")->fetch_all();
$categoryName_sql = $msql->query("SELECT `name` FROM `category`")->fetch_all();

$tovar_sql = $msql->query("SELECT `id_category`, `id`,`name`, `count` FROM `tovar`")->fetch_all();
$tovarNames_sql = $msql->query("SELECT `name` FROM `tovar`")->fetch_all();

// перестройка ид
$mySqliConnect->resetTableId('category');
$mySqliConnect->resetTableId('tovar');
?>

<body>

<div class='row w-100 justify-content-center'>
    <button type="button" class='btn btn-success col-2 font-weight-bold' name='home' onclick="location.href = 'index.php'">На главную</button>
</div>

<section id="add_tovar">

<div class="container">
    <div class="row justify-content-center">
        <form method="post" name="from_add" class="text-center col-6 mt-5" enctype="multipart/form-data">

            <div class='mx-1 my-2 text-center col-12'>
                <label for='category' class='col-10 col-sm-12 col-md-10  text-dark rounded h5 font-weight-bold bg-white d-block'>Выбирите категорию:
                <input id='input_category' cat-id='' name='category' list='category' placeholder='категория' class="col-12">
                <datalist id='category' onautocomplete='false'>
                    <?php
                        foreach ($category_sql as $key=>$category) {
                            echo "<option name='' id='$category[0]' class='category' value='$category[1]'></option>";
                        }
                    ?>
                </datalist>
            </div>
<!--            загрузка картинок-->
            <div class='mx-1 my-2 text-center col-12'>
                <input type="file" name="image" class="filestyle"  data-badge="true" data-placeholder="<= загрузить фото">
            </div>

            <div class='mx-1 my-2 text-center col-12'>
                <label for='sort' class='col-10 col-sm-12 col-md-10  text-dark rounded h5 font-weight-bold bg-white d-block' >Название сорта:
                <input type='text' name='sort' id='sort' placeholder='имя' class="col-12 ">
            </div>
            <div class='mx-1 my-2 text-center col-12'>
                <label for='description' class='col-10 col-sm-12 col-md-10  text-dark rounded h5 font-weight-bold bg-white d-block'>Описание:
                    <textarea id='description' name='description' maxlength='1000' cols='' rows='6' placeholder='краткое описание' class="col-12"></textarea>
            </div>
            <div class='mx-1 my-2 text-center col-12'>
                <label for='weight' class='col-4 col-sm-10 col-md-10 text-dark rounded h5 font-weight-bold bg-white d-block'>Вес:
                    <input type='text' id='weight' name='weight' placeholder='вес грамм' class="col-12">
            </div>
            <div class='mx-1 my-2 text-center col-12'>
                <label for='weight' class='col-10 col-sm-12 col-md-10 text-dark rounded h5 font-weight-bold bg-white d-block'>Количество:
                    <input type='text' id='count' name='count' placeholder='количество шт.' class="col-12">
            </div>
            <div class='mx-1 my-2 text-center col-12'>
                <label for='price' class='col-10 col-sm-12 col-md-10 text-dark rounded h5 font-weight-bold bg-white d-block'>Цена за 1 шт:
                    <input type='text' id='price' name='price' placeholder='цена грн.' class="col-12">
            </div>

            <input type='text' id='' name='' hidden value=''>
            <button id='add' name='add' class='bg-white border border-info col-5 col-sm-6 text-dark text-center font-weight-bold h3 rounded-pill py-2 mr-5' type='submit'>Добавить</button>
        </form>
    </div>
</div>
</section>

<?php
// перестройка ид
$mySqliConnect->resetTableId('category');
$mySqliConnect->resetTableId('tovar');

//если кнопка нажата
if(isset($_POST["add"])) {
    $catName = $_POST['category'];
    $tovName = $_POST['sort'];
    $tovDescription = $_POST['description'];
    $tovWeight = $_POST['weight'];
    $tovCount = $_POST['count'];
    $tovPrice = $_POST['price'];

//===ЗАПОЛНЯЕМ КАТЕГОРИИ===
//    проверяем есть ли такая категория в бд
    $checkCategory = $msql->query("SELECT `id`,`name` FROM `category` WHERE `name`='$catName'")->fetch_array();
//    если в категориях БД нет такой категории
    if (empty($checkCategory)){
//        добавляєм категорию
        $add_category = $msql->query("INSERT INTO `category` (`name`) VALUES ('$catName')");
//        получаем ид новой категории для подкатегории
        $categoryIdSql = $msql->query("SELECT `id` FROM `category` WHERE `name`='$catName'")->fetch_array();
        $categoryId = $categoryIdSql[0];
        $checkCategory = 'added category';
        $mySqliConnect->resetTableId('category');
    } else {
//        если категория есть получаем ее ид
        $categoryId = $checkCategory[0];
//        Обновления вместо редактирования
        $msql->query("UPDATE `category` SET `name`= '$catName' WHERE `id`='$categoryId' ");
    }

//===ЗАПОЛНЯЕМ ФОТО===
// установка временной зоны по умолчанию. Доступно с PHP 5.1
    date_default_timezone_set('Europe/Kiev');

    $output_dir = "картинки/товар";/* Path for file upload */
    $today = date("d-m-Y H:i:s");
    $now =  str_replace(' ','_',$today);
    if (!empty($_FILES['image']['name'])) {
        $fileName = explode(".", $_FILES['image']['name'])[0];
        $fileExt = explode(".", $_FILES['image']['name'])[1];

        $NewImageName = $tovName.".".$fileExt;
        /* Try to create the directory if it does not exist */
        if (!file_exists($output_dir))
        {
            if (!mkdir($output_dir, 0777) && !is_dir($output_dir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $output_dir));
            }
        }
        $full_path = $output_dir."/".$NewImageName;
        move_uploaded_file($_FILES["image"]["tmp_name"], $full_path);
    } else {
        $full_path = '';
    }
    //===ЗАПОЛНЯЕМ ТОВАР===
    $mySqliConnect->resetTableId('tovar');
    $checkTovar = $msql->query("SELECT `name` FROM `tovar` WHERE `name`='$tovName'")->fetch_array();
    if (empty($checkTovar)){
        $add_tovar = $msql->query("INSERT INTO `tovar` (`id_category`,`photo`,`name`,`description`,`weight`,`count`,`price`) VALUES ('$categoryId','$full_path','$tovName','$tovDescription','$tovWeight','$tovCount', '$tovPrice')");
        $mySqliConnect->resetTableId('tovar');
    } else {
//        обновление вместо редактирования
        $msql->query("UPDATE `tovar` SET `id_category`= '$categoryId', `foto`='$full_path',`sort_name`='$tovName',`description`='$tovDescription',`weight`='$tovWeight',`count`='$tovCount',`price`='$tovPrice' WHERE `name`='$tovName'");
    }
}
?>

<script src="scripts/jquery-3.6.0.min.js"></script>
<script src="scripts/jquery.cookie.js"></script>
<script src="scripts/bootstrap.bundle.min.js"></script>
<script src="scripts/bootstrap-filestyle.min.js"></script>
<script src="scripts/custom/add.js"></script>

</body>

</html>