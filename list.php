<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Создание товара</title>
    <link rel="stylesheet" href="styles/bootstrap.min.css">
    <link rel="stylesheet" href="styles/bootstrap-grid.min.css">
    <link rel="stylesheet" href="styles/custom/ordering.css">
</head>

<body>

<span class="noprint">
<div class='row w-100 justify-content-center'>
    <a class ="print-doc mx-3" href="javascript:(print());"> <img src="картинки/ico/print.png" alt=”Распечатать” width="50px" height="50px"></a>
    <button type="button" class='btn btn-success col-2 font-weight-bold' name='home' onclick="location.href = 'index.php'">На главную</button>
</div>
</span>

<?php
require_once(__DIR__ . '/sql/connection.php');

// Connection to MySQLI.
$mySqliConnect = new MySqliConnect();
$msql = $mySqliConnect->msql();

$categorySQL = $msql->query('SELECT * FROM `category` ')->fetch_all();
$tovarSQL = $msql->query('SELECT * FROM `tovar` ')->fetch_all();
?>
<table class='table table-success table-hover'>
    <caption class='text-white h4 bg-dark text-center'></caption>
    <thead class='table-dark '>
    <tr class=''><th scope='col' colspan='7' class='h4 text-white bg-dark text-center'>Список товаров:</th></tr>
    <tr class=''>
        <th scope='col' class='rounded-pill order-head text-center'>№:</th>
        <th scope='col' class='rounded-pill order-head text-center'>Наименование:</th>
        <th scope='col' class='rounded-pill order-head text-center'>Описание:</th>
        <th scope='col' class='rounded-pill order-head text-center'>Вес:</th>
        <th scope='col' class='rounded-pill order-head text-center'>Количество:</th>
        <th scope='col' class='rounded-pill order-head text-center'>Цена:</th>
    </tr>
    </thead>
    <tbody>
<?php
foreach ($tovarSQL as $key => $tovar) {
echo "
       <tr>
        <thead class='table-dark'>
        <td width='30px'>$tovar[1]</td>
        <td width='300px'>$tovar[3]</td>
        <td width='600px'>$tovar[4]</td>
        <td class='text-center' width='100px'>$tovar[5]</td>
        <td class='text-center'>$tovar[6]</td>
        <td class='text-center'>$tovar[7]</td>
        </thead>
       </tr>
";
}
?>
    </tbody>
</table>

</body>


<script src="scripts/jquery-3.6.0.min.js"></script>
<script src="scripts/bootstrap.bundle.min.js"></script>
<script src="scripts/jquery.cookie.js"></script>

</body>

</html>
